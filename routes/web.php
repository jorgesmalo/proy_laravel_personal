<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('cuotas','CuotasController@index')->name('cuotas.index');
Route::get('cuotas/create','CuotasController@create')->name('cuotas.create');
Route::post('cuotas/create','CuotasController@store')->name('cuotas.store');
Route::get('cuotas/edit/{id}','CuotasController@edit')->name('cuotas.edit');
Route::put('cuotas/update/{id}','CuotasController@update')->name('cuotas.update');
Route::delete('cuotas/destroy/{id}','CuotasController@destroy')->name('cuotas.destroy');
Route::get('cuotas/show/{id}','CuotasController@show')->name('cuotas.show');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/cerrarSesion', 'Auth\LoginController@logout')->name('cerrarSesion');

Route::get('/dynamic_pdf', 'DynamicPDFController@index');
Route::get('/dynamic_pdf/pdf', 'DynamicPDFController@pdf');